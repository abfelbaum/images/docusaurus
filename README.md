# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

### Installation

```
$ npm install
```

### Local Development

Change the `docusaurus.config.js` file and add your local docs folder that should be compiled to `preset[].docs.path`. For example: `/home/itc/infrastructure/docs/docs`.

```
$ DOCUSAURUS_TITLE=test DOCUSAURUS_URL='http://example.com/' DOCUSAURUS_LANGUAGE=en DOCUSAURUS_PROJECT_URL=http://asd npm run start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

You can open your docs at [http://localhost:3000/](http://localhost:3000/). For example if your docs folder contains a file `Signoz.mdx` you can open it under [http://localhost:3000/signoz](http://localhost:3000/signoz).
