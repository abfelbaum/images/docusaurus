/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/preset-classic').ThemeConfig} */
const themeConfig = {
  navbar: {
    title: process.env.DOCUSAURUS_TITLE,
    items: [
      {
        href: 'https://rwthapp.pages.rwth-aachen.de/documentation/',
        label: 'Übersicht',
        position: 'left'
      },
      {
        href: 'https://rwthapp.pages.rwth-aachen.de/misc/documentation/',
        label: 'Team',
        position: 'left'
      },
      {
        href: process.env.DOCUSAURUS_PROJECT_URL,
        label: 'GitLab',
        position: 'right',
      },
    ],
  }
};

module.exports = themeConfig;
