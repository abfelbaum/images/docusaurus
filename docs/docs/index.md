---
displayed_sidebar: default
---

# Docusaurus

The [`registry.git.rwth-aachen.de/rwthapp/misc/container-images/docusaurus`](https://git.rwth-aachen.de/rwthapp/misc/container-images/docusaurus/container_registry) image contains docusaurus to build the documentation in our projects.

## Image Building

The image takes the files from the repository and puts them, with their dependencies in the docker container.

## Usage

Usage and configuration is documented [here](https://rwthapp.pages.rwth-aachen.de/misc/ci/documentation).