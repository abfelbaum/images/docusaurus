// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const themeConfig = require('./themeconfig.js');
const url = new URL(process.env.DOCUSAURUS_URL);

themeConfig.navbar.logo = {
  alt: 'IT Center Logo',
  src: 'img/logo.png',
},

themeConfig.prism = {
  theme: lightCodeTheme,
  darkTheme: darkCodeTheme,
  additionalLanguages: [
    'aspnet',
    'awk',
    'bash',
    'csharp',
    'cshtml',
    'css',
    'csv',
    'docker',
    'dot',
    'editorconfig',
    'fsharp',
    'git',
    'hcl',
    'ignore',
    'ini',
    'java',
    'javascript',
    'json',
    'jsx',
    'log',
    'markdown',
    'powershell',
    'regex',
    'scss',
    'solution-file',
    'sql',
    'systemd',
    'toml',
    'typescript',
    'yaml'
  ]
};

themeConfig.footer = {
  style: 'dark',
  copyright: `Copyright © ${new Date().getFullYear()} RWTH Aachen University<br/>Built with Docusaurus`,
};

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: process.env.DOCUSAURUS_TITLE,
  favicon: 'img/logo.png',

  // Set the production url of your site here
  url: url.origin,
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: url.pathname,

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: process.env.DOCUSAURUS_LANGUAGE,
    locales: [process.env.DOCUSAURUS_LANGUAGE],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          // path: '/path/to/docs/docs',
          routeBasePath: '/',
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: process.env.DOCUSAURUS_EDITURL,
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
          editCurrentVersion: true
        },
        blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig
};

module.exports = config;
