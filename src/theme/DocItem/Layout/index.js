import React from 'react';
import Layout from '@theme-original/DocItem/Layout';
import {useDoc} from '@docusaurus/theme-common/internal';

export default function LayoutWrapper(props) {
    const doc = useDoc();
    return (
        <div className={doc.frontMatter.full_width ? "full-width" : "container"}>
            <Layout {...props} />
        </div>
    );
}
