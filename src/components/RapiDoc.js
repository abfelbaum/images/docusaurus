import BrowserOnly from "@docusaurus/BrowserOnly";
import {useColorMode} from '@docusaurus/theme-common';
import React from "react";


export default function RapiDoc(props) {
  const {colorMode} = useColorMode();
  const primaryColor = colorMode === 'dark' ? '#0a84ff' : '#00549f';
  const backgroundColor = colorMode === 'dark' ? '#2a2b2c' : '#fafafa';

  return (
    <BrowserOnly fallback={<div>Loading...</div>}>
      {() => {
        require('rapidoc');
        return <rapi-doc
          sort-tags="true"
          persist-auth="true"
          show-method-in-nav-bar="as-colored-block"
          use-path-in-nav-bar="true"
          render-style="focused"
          theme={colorMode === 'dark' ? 'dark' : 'light'}
          allow-spec-url-load="false"
          allow-spec-file-load="false"
          allow-spec-file-download="true"
          schema-hide-read-only="never"
          schema-hide-write-only="never"
          show-header="false"
          show-curl-before-try="true"
          schema-style="table"
          show-components="true"
          schema-description-expanded="true"
          default-schema-tab="schema"
          spec-url={props.spec}
          load-fonts="false"
          regular-font="Arial, Helvetica, sans-serif"
          primary-color={primaryColor}
          bg-color={backgroundColor}
          nav-bg-color={backgroundColor}
          style={{height: 'auto'}}
        />
      }}
    </BrowserOnly>
  )
}